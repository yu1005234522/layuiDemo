<?php

/* 
 * 基础代码管理前台
 * 
 */

include_once("inc/auth.inc.php");
include_once("inc/utility_all.php");
include_once 'inc/mysqlDB.class.php';



//搜索框
if($_GET['name']){
    $dataUrl = 'getBaseData.php?name='.$_GET['name'];
}else{
    $dataUrl = 'getBaseData.php';
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="gbk">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="<?=MYOA_STATIC_SERVER?>/static/layui/css/layui.css"  media="all">
</head>

<body class="bodycolor">
    <!-- 补丁 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--layui-fluid 布局容器不固定宽度 100%自适应 -->
    <div class="layui-fluid" style="padding: 6px;">
        <div class="layui-row">
            <div class="layui-col-md12" >
                <div style="float: left;padding-right: 6px;">
                    <input style="width: 200px;" type="text" name="name" id="name" required  lay-verify="required" placeholder="请输入代码名称" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-btn-container">
                    <button class="layui-btn"  id="selBtn">查  询</button>
                    <button class="layui-btn layui-btn-normal" lay-filter="btnAdd" onclick="addCode()">添加</button>
                    <button class="layui-btn layui-btn-danger" id="delBtn">删除</button>
                </div>
            </div>
        </div>
        <!--底部数据展示区-->
        <div class="layui-row">
            <div class="layui-col-md12">
                <div>
                    <table class="layui-table" lay-data="{height: 'full-100',url:'<?php echo $dataUrl;?>', page:true, id:'baseTableId'}" lay-filter="baseTable">
                        <thead>
                            <tr>
                                <th lay-data="{type:'checkbox', fixed: 'left'}"></th>
                                <th lay-data="{field:'id', width:80, sort: true, fixed: true}">ID</th>
                                <th lay-data="{field:'code',width:200}">代码编号</th>
                                <th lay-data="{field:'name', width:200,edit: 'text'}">代码名称</th>
                                <th lay-data="{fixed: 'right', width:120, align:'center', toolbar: '#barBaseTable'}"></th>
                            </tr>
                        </thead>
                    </table>
                    <script type="text/html" id="barBaseTable">
                        <a class="layui-btn layui-btn-xs" lay-event="edit" lay-event="edit">可选项管理</a>
                        <!--
                        <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                       -->
                    </script>
                </div>
            </div>
        </div>
    </div>   
    
    <!--添加代码 弹出层 dom-->
    <div style="width: 360px;height: 200px; display: none;margin-top: 10px;" id="addDom">
        <form class="layui-form" action="" accept-charset="utf-8">
            <div class="layui-form-item">
            
                <label class="layui-form-label">代码编号</label>
                <div class="layui-input-block">
                    <input style="width: 200px;" type="text" name="code" required  lay-verify="required" placeholder="请输入唯一编码,禁止重复" autocomplete="off" class="layui-input">
                </div>
            
            </div>
            <div class="layui-form-item">

                <label class="layui-form-label">名称</label>
                <div class="layui-input-block">
                    <input style="width: 200px;" type="text" name="name" required  lay-verify="required" placeholder="请输入代码名称" autocomplete="off" class="layui-input">
                </div>
            </div>
        
            <div class="layui-form-item">
                <div class="layui-input-block">
                  <button class="layui-btn" lay-submit lay-filter="subBtn">立即提交</button>
                  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </div>
    
    <script type="text/javascript" src="<?=MYOA_JS_SERVER?>/static/layui/layui.js"></script>
    <script type="text/javascript">
       
        var addIndex;//全局化打开的层id便于关闭
        
        //添加按钮打开一个层
        function addCode(){
            layui.use(['layer','jquery'],function (){
                var layer = layui.layer;
                var $ = layui.jquery;
                addIndex = layer.open({
                    type:1,
                    title:'添加代码',
                    area: ['400px', '300px'],
                    shade:0.3,
                    content:$('#addDom')
                });
            })
        }
        
        //删除按钮
        layui.use(['table','jquery'],function (){
            var table = layui.table;
            var $ = layui.jquery;
            
            //数据明细工具条单击
            table.on('tool(baseTable)', function(obj){
                var data = obj.data;
                if(obj.event === 'edit'){
                    layer.msg('ID：'+ data.id + ' 的编辑操作');
                }
            });
            
            //删除单击
            $('#delBtn').on('click', function(){
                var checkStatus = table.checkStatus('baseTableId');
                var data = checkStatus.data;
                if(data.length == 0){
                    layer.alert('您没有选中任何数据！');
                    return;
                }
                layer.confirm('真的要删除吗？',{icon: 3, title:'警告'}, function(index){
                    //拆分选中的数据提取id
                    var newData = new Array();
                    for($i=0;$i<data.length;$i++){
                        var code = data[$i];
                        newData[$i]=code.id;
                    }
                    //转换数组为 json 用于post
                    var str = JSON.stringify(newData);
                    $.ajax({
                        type:'POST',
                        url:'delBase.php',
                        data:{data : str},
                        success:function(msg){
                            if(msg.status == 1){
                                layer.alert('删除成功！',function(){
                                    window.location.reload();//刷新页面
                                });
                            }
                        }
                    })
                    layer.close(index);
                });
            });
            
            //监听单元格编辑
            table.on('edit(baseTable)', function(obj){
                var value = obj.value //得到修改后的值
                ,data = obj.data //得到所在行所有键值
                ,field = obj.field; //得到字段
                layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改为：'+ value);
            });
             
        });
        
        //监听弹出层的 提交按钮
        layui.use(['form','jquery'], function(){
            var form = layui.form;
            var $ = layui.jquery;
            form.on('submit(subBtn)', function(data){
                //layer.msg(JSON.stringify(data.field));
                $.ajax({
                    type: 'POST',
                    url: 'addBaseData.php',
                    data: data.field,
                    success: function(msg){
                        if(msg.status == 1){
                            layer.close(addIndex);
                            layer.alert('添加成功！',function(index){
                                window.location.reload();//刷新页面
                                layer.close(index);
                            });
                        }else if(msg.status == -1){
                            layer.alert('代码编号重复请修改');
                        }
                    },
                });
              return false;
            });
        });
        
        //查询按钮
        layui.use(['jquery'],function(){
            var $ = layui.jquery;
            $('#selBtn').on('click',function(){
                var val = $('#name').val();
                if(!val){layer.alert('请输入代码名称');return;}
                window.location.href = 'f1.php?name='+val;
            })
        })
        
              
    </script>
    
    
</body>

</html>
