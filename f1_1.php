<?php

/* 
 * 基础代码管理前台
 * 
 */

include_once("inc/auth.inc.php");
include_once("inc/utility_all.php");
include_once 'inc/mysqlDB.class.php';

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="gbk">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="<?=MYOA_STATIC_SERVER?>/static/layui/css/layui.css"  media="all">
</head>

<body class="bodycolor">
    <!-- 补丁 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--layui-fluid 布局容器不固定宽度 100%自适应 -->
    <div class="layui-fluid" style="padding: 6px;">
        <div class="layui-row">
            <div class="layui-col-md12" >
                <div style="float: left;padding-right: 6px;">
                    <input style="width: 200px;" type="text" name="title" required  lay-verify="required" placeholder="请输入代码名称" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-btn-container">
                    <button class="layui-btn">查  询</button>
                    <button class="layui-btn layui-btn-normal" lay-filter="btnAdd" onclick="addCode()">添加</button>
                    <button class="layui-btn" data-type="getCheckData">获取选中行数据</button>
                </div>
            </div>
        </div>
        <!--底部数据展示区-->
        <div class="layui-row">
            <div class="layui-col-md12">
                <div>
                    <table class="layui-table" lay-data="{height: 'full-100',url:'getBaseData.php', page:true, id:'baseTableId'}" lay-filter="baseTable">
                        <thead>
                            <tr>
                                <th lay-data="{type:'checkbox', fixed: 'left'}"></th>
                                <th lay-data="{field:'id', width:80, sort: true, fixed: true}">ID</th>
                                <th lay-data="{field:'code',width:200}">代码编号</th>
                                <th lay-data="{field:'name', width:200}">代码名称</th>
                                <th lay-data="{fixed: 'right', width:120, align:'center', toolbar: '#barBaseTable   '}"></th>
                            </tr>
                        </thead>
                    </table>
                    <script type="text/html" id="barBaseTable">
                        <a class="layui-btn layui-btn-xs" lay-event="edit">可选项管理</a>
                        <!--
                        <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                        -->
                    </script>
                </div>
            </div>
        </div>
    </div>   
    
    <!--添加代码 弹出层 dom-->
    <div style="width: 360px;height: 200px; display: none;margin-top: 10px;" id="addDom">
        <form class="layui-form" action="" accept-charset="utf-8">
            <div class="layui-form-item">
            
                <label class="layui-form-label">代码编号</label>
                <div class="layui-input-block">
                    <input style="width: 200px;" type="text" name="code" required  lay-verify="required" placeholder="请输入唯一编码,禁止重复" autocomplete="off" class="layui-input">
                </div>
            
            </div>
            <div class="layui-form-item">

                <label class="layui-form-label">名称</label>
                <div class="layui-input-block">
                    <input style="width: 200px;" type="text" name="name" required  lay-verify="required" placeholder="请输入代码名称" autocomplete="off" class="layui-input">
                </div>
            </div>
        
            <div class="layui-form-item">
                <div class="layui-input-block">
                  <button class="layui-btn" lay-submit lay-filter="subBtn">立即提交</button>
                  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </div>
    
    <script type="text/javascript" src="<?=MYOA_JS_SERVER?>/static/layui/layui.js"></script>
    <script type="text/javascript">
        var layer,$,addIndex;
        layui.use(['jquery','layer','table'], function(){
            layer = layui.layer;
            $ = layui.jquery;
            table = layui.table;
        });
        
        function addCode(){
            addIndex = layer.open({
                type:1,
                title:'添加代码',
                area: ['400px', '300px'],
                shade:0.3,
                content:$('#addDom')
            });
        }

        
        
        
        //监听提交
        layui.use('form', function(){
            form = layui.form;
            form.on('submit(subBtn)', function(data){
                //layer.msg(JSON.stringify(data.field));
                $.ajax({
                    type: 'POST',
                    url: 'addBaseData.php',
                    data: data.field,
                    success: function(msg){
                        if(msg.status == 1){
                            layer.close(addIndex);
                            layer.alert('添加成功！',function(index){
                                window.location.reload();//刷新页面
                                layer.close(index);
                            });
                        }else if(msg.status == -1){
                            layer.alert('代码编号重复请修改');
                        }
                    },
                });
              return false;
            });
        });
        
        
        
        
                      
    </script>
    
    
</body>

</html>
